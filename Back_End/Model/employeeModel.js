const mongoose = require('mongoose');

var Employee = mongoose.model('Employee', {
  name: {
    type: String
  },
  address: {
    type: String
  },
  number: {
    type: Number
  },
  age: {
    type: Number
  },
  organisation: {
    type: String
  },
  dob: {
    type: Date
  },
  experience: {
    type: Number
  },
  username: {
    type: String
  }

});

module.exports = {
  Employee: Employee
};
