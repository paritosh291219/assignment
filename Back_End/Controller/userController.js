const express = require("express");
const aes256 = require("aes256");
var ObjectId = require('mongoose').Types.ObjectId;
var router = express.Router();

var {
  User
} = require("../Model/userModel");
var {
  Employee
} = require("../Model/employeeModel");

var key = "Ah5Su2JWUrIyzBGO7fvFEtblSqCZknQZ";

router.post("/register", (req, res) => {
  console.log(req.body.username);
  var query = req.body.username;

  req.body.password = aes256.encrypt(key, req.body.password);

  User.findOne({
      username: query
    },
    (err, doc) => {
      if (err) {
        console.log("Error");
      }
      if (doc) {
        console.log("Username exist");
        return res.status(400).send(`Username Exist`);
      } else {
        var user = new User({
          name: req.body.name,
          address: req.body.address,
          username: req.body.username,
          age: req.body.age,
          organisation: req.body.organisation,
          dob: req.body.dob,
          experience: req.body.experience,
          password: req.body.password
        });

        user.save((err, doc) => {
          if (!err) {
            res.send(doc);
            console.log("Success insert");
          } else console.log("fail :" + JSON.stringify(err, undefined, 2));
        });
      }
    }
  );
});

router.post("/validate", (req, res) => {
  console.log(req.body.username);

  var query = req.body.username;
  var query2 = req.body.password;

  User.findOne({
      username: query
    },
    (err, doc) => {
      if (err) {
        console.log("Error");
      }
      if (doc) {
        console.log("Username exist");

        var decryptedPassword = aes256.decrypt(key, doc.password);

        if (decryptedPassword == query2) {
          console.log("Authentication Success");
          res.status(200).send(doc);
        } else return res.status(401).send(`Authentication Failed`);
      } else {
        console.log("Authentication Failed");
        return res.status(401).send(`Authentication Failed`);
      }
    }
  );
});

router.get('/allEmployee', (req, res) => {
  Employee.find((err, docs) => {
    if (!err) {
      res.send(docs);
    } else {
      console.log('Error in Retriving Employees :' + JSON.stringify(err, undefined, 2));
    }
  });
});

router.get('/:id', (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No record with given id : ${req.params.id}`);

  Employee.findById(req.params.id, (err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log('Error in Retriving Employee :' + JSON.stringify(err, undefined, 2));
    }
  });
});

router.post('/saveEmployee', (req, res) => {
  var emp = new Employee({
    name: req.body.name,
    address: req.body.address,
    number: req.body.number,
    age: req.body.age,
    organisation: req.body.organisation,
    dob: req.body.dob,
    experience: req.body.experience
  });
  emp.save((err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log('Error in Employee Save :' + JSON.stringify(err, undefined, 2));
    }
  });
});

router.put('/:id', (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No record with given id : ${req.params.id}`);

  var emp = {
    name: req.body.name,
    address: req.body.address,
    number: req.body.number,
    age: req.body.age,
    organisation: req.body.organisation,
    dob: req.body.dob,
    experience: req.body.experience
  };
  Employee.findByIdAndUpdate(req.params.id, {
    $set: emp
  }, {
    new: true
  }, (err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log('Error in Employee Update :' + JSON.stringify(err, undefined, 2));
    }
  });
});

router.delete('/:id', (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No record with given id : ${req.params.id}`);

  Employee.findByIdAndRemove(req.params.id, (err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log('Error in Employee Delete :' + JSON.stringify(err, undefined, 2));
    }
  });
});

module.exports = router;
