import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";

import { CommonService } from "../Shared/common.service";
import { Employee } from "../shared/employee.model";

declare var M: any;

@Component({
  selector: "app-employee",
  templateUrl: "./employee.component.html",
  styleUrls: ["./employee.component.css"],
  providers: [CommonService]
})
export class EmployeeComponent implements OnInit {
  constructor(private employeeService: CommonService) {}

  public resetValue: string = `<p class="lead">I, @Name age @Age resident of $Address working with
  $Organisation having experience of
  $Experience years.</p> <hr class="my-4">
  <div style="text-align: left; padding-top: 15px;">
    <p>Thanks,</p>
    <p>Name : @Name</p>
    <p>Age : @Age</p>
    <p>Mobile : $Number</p>
    <p>Date of Birth : $DOB</p>
  </div>`;

  public value: string = `<p class="lead">I, @Name age @Age resident of $Address working with
  $Organisation having experience of
  $Experience years.</p> <hr class="my-4">
  <div style="text-align: left; padding-top: 15px;">
    <p>Thanks,</p>
    <p>Name : @Name</p>
    <p>Age : @Age</p>
    <p>Mobile : $Number</p>
    <p>Date of Birth : $DOB</p>
  </div>`;

  ngOnInit() {
    this.resetForm();
    this.refreshEmployeeList();
  }

  resetForm(form?: NgForm) {
    if (form) form.reset();
    this.employeeService.selectedEmployee = {
      _id: "",
      name: "",
      number: null,
      address: "",
      age: null,
      organisation: "",
      dob: new Date(),
      experience: null
    };
    this.value = this.resetValue;
  }

  onSubmit(form: NgForm) {
    if (form.value._id == "") {
      this.employeeService.postEmployee(form.value).subscribe(res => {
        this.resetForm(form);
        this.refreshEmployeeList();
        M.toast({ html: "Saved successfully", classes: "rounded" });
      });
    } else {
      this.employeeService.putEmployee(form.value).subscribe(res => {
        this.resetForm(form);
        this.refreshEmployeeList();
        M.toast({ html: "Updated successfully", classes: "rounded" });
      });
    }
    this.value = this.resetValue;
  }

  refreshEmployeeList() {
    this.employeeService.getEmployeeList().subscribe(res => {
      this.employeeService.employees = res as Employee[];
    });
  }

  onEdit(emp: Employee) {
    this.employeeService.selectedEmployee = emp;
    this.value = this.value
      .replace(new RegExp(`@Name`, `g`), emp.name)
      .replace("$Name", emp.name)
      .replace("$Address", emp.address)
      .replace("$Number", emp.number.toString())
      .replace(new RegExp(`@Age`, `g`), emp.age.toString())
      .replace("$Organisation", emp.organisation)
      .replace("$Experience", emp.experience.toString())
      .replace("$DOB", emp.dob.toString());
  }

  onDelete(_id: string, form: NgForm) {
    if (confirm("Are you sure to delete this record ?") == true) {
      this.employeeService.deleteEmployee(_id).subscribe(res => {
        this.refreshEmployeeList();
        this.resetForm(form);
        M.toast({ html: "Deleted successfully", classes: "rounded" });
      });
    }
  }
  onChangeName(change: string) {
    this.value = this.value.replace(new RegExp(`@Name`, `g`), change);
  }
  onChangeAdd(change: string) {
    this.value = this.value.replace("$Address", change);
  }
  onChangeNum(change: string) {
    this.value = this.value.replace("$Number", change);
  }
  onChangeAge(change: string) {
    this.value = this.value.replace(new RegExp(`@Age`, `g`), change);
  }
  onChangeOrg(change: string) {
    this.value = this.value.replace("$Organisation", change);
  }
  onChangeExp(change: string) {
    this.value = this.value.replace("$Experience", change);
  }
  onChangeDob(change: any) {
    console.log(change);
    this.value = this.value.replace("$DOB", change);
  }
}
