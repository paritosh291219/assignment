import { Component, OnInit, Renderer } from "@angular/core";
import { CommonService } from "../Shared/common.service";
import { NgForm, FormBuilder, Validators, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { User } from "../Shared/user.model";
import { SharedObjectService } from "../Shared/shared-object.service";

export var username: any;
declare var M: any;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [CommonService]
})
export class LoginComponent implements OnInit {
  constructor(
    private sharedObject: SharedObjectService,
    private commonService: CommonService,
    private _router: Router
  ) {}

  ngOnInit() {
    this.commonService.selectedUser = new User();
  }

  onSubmit(form: NgForm): any {
    this.commonService.Validate(form.value).subscribe(
      res => {
        this.commonService.selectedUser = res;
        username = this.commonService.selectedUser.username;
        this.sharedObject.sharedUser = this.commonService.selectedUser;
        console.log(this.commonService.selectedUser.username);
        this._router.navigate(["/employee"]);
      },
      error => {
        console.log(error);
        M.toast({ html: "Invalid Credentials", classes: "rounded" });
      }
    );
  }
}
