import { Component, OnInit } from "@angular/core";
import { CommonService } from "../Shared/common.service";
import { username } from "../login/login.component";
import { SharedObjectService } from "../Shared/shared-object.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-template",
  templateUrl: "./template.component.html",
  styleUrls: ["./template.component.css"]
})
export class TemplateComponent implements OnInit {
  username = username;
  user: any;

  constructor(
    private sharedObject: SharedObjectService,
    private commonService: CommonService,
    private _router: Router
  ) {}

  ngOnInit() {
    this.user = this.sharedObject.sharedUser;
  }

  logout() {
    this._router.navigate(["/login"]);
  }
}
