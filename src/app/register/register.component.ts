import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { CommonService } from "../Shared/common.service";
import { Router } from "@angular/router";
import { User } from "../Shared/user.model";

declare var M: any;

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  constructor(private commonService: CommonService, private _router: Router) {}

  ngOnInit() {
    this.commonService.selectedUser = new User();
  }

  onSubmit(form: NgForm): any {
    this.commonService.Register(form.value).subscribe(
      res => {
        console.log(res);
        this._router.navigate(["/login"]);
      },
      error => {
        console.log(error);
        M.toast({ html: "User Already exist", classes: "rounded" });
      }
    );
  }
}
