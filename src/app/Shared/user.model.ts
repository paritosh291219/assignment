export class User {
  _id: string;
  name: string;
  address: string;
  username: number;
  age: number;
  organisation: string;
  dob: Date;
  experience: number;
  password: string;
}
