import { TestBed } from '@angular/core/testing';

import { SharedObjectService } from './shared-object.service';

describe('SharedObjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedObjectService = TestBed.get(SharedObjectService);
    expect(service).toBeTruthy();
  });
});
