export class Employee {
  _id: string;
  name: string;
  address: string;
  number: number;
  age: number;
  organisation: string;
  dob: Date;
  experience: number;
}
