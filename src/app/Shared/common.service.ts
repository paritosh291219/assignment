import { Injectable } from "@angular/core";
import { User } from "./user.model";
import { Employee } from "./employee.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CommonService {
  selectedUser: User;
  selectedEmployee: Employee;
  employees: Employee[];
  baseURL = "http://localhost:5000/user";

  constructor(private http: HttpClient) {}

  Validate(user: User): Observable<any> {
    return this.http.post(this.baseURL + "/validate", user);
  }

  Register(user: User): Observable<any> {
    return this.http.post(this.baseURL + "/register", user);
  }

  postEmployee(emp: Employee) {
    return this.http.post(this.baseURL + "/saveEmployee", emp);
  }

  getEmployeeList() {
    return this.http.get(this.baseURL + "/allEmployee");
  }

  putEmployee(emp: Employee) {
    return this.http.put(this.baseURL + `/${emp._id}`, emp);
  }

  deleteEmployee(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }
}
