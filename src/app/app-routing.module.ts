import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TemplateComponent } from "./template/template.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { EmployeeComponent } from "./employee/employee.component";

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "template", component: TemplateComponent },
  { path: "employee", component: EmployeeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
